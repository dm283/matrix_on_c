#include <stdio.h>

void display_matrix(int n, int m, int mx[][m]);

int main(){
    int s, n, m, i, j;

    // main menu
    system("cls");
    printf("1. Enter matrix");
    printf("\n2. Display matrix");
    printf("\n3. Add matrix");
    printf("\n4. Multiply matrix by scalar");
    printf("\n5. Multiply matrix by matrix");
    printf("\n6. Exit");
    printf("\n[n]: ");
    scanf("%d", &s);


    // enter matrix
    system("cls");
    printf("\nEnter number of matrix rows and columns (via 'space'): ");
    scanf("%d", &n); scanf("%d", &m);

    int mx[n][m];

    for (i = 0; i < n; i++){
        printf("Enter elements of %d row of matrix (via 'space'): ", i+1);
        for (j = 0; j < m; j++){
            scanf("%d", &mx[i][j]);
        }
    }

    display_matrix(n, m, mx);
}

void display_matrix(int n, int m, int mx[][m]){
    int i, j;

    for (i = 0; i < n; i++){
        for (j = 0; j < m; j++){
            printf("%d ", mx[i][j]);
        }
        printf("\n");
    }
}
